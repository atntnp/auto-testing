*** Settings ***
Library           Libraries.py
Library           Browser
Library           OperatingSystem
#Library          Selenium2Library
Library           DatabaseLibrary
Suite Setup       Chrome Setup 
Suite Teardown    Browser.Close Browser
Resource          LogIn_FE.robot

*** Variables ***
${admin_username}           acloud-admin@disposeamail.com
${admin_password}           acloudadmin
${operator_username}        acloud-operator@disposeamail.com
${operator_password}        acloudoperator
${viewer_username}          acloud-viewer@disposeamail.com
${viewer_password}          acloudviewer
${email_1}                  test@gmail.com
${email_2}                  testAtgmail.com
${email_3}                  test@gmailcom
${email_4}                  @gmail.com
${email_5}                  @gmail
${admin}                    'Admin'
${operator}                 'Operator'
${viewer}                   'Viewer'
${create_user_name}         Test_for_Robot
${search_role}              viewer
${invalid_delete_input1}    delete
${invalid_delete_input2}    DeLete
${invalid_delete_input3}    OK
${valid_delete_input}       DELETE
${index}                    0

*** Keywords ***
Chrome Setup
    Browser.Open Browser      https://demo.fleet-guard.com    chromium
    Set Viewport Size         width=1536                      height=712
    Set LocalStorage value
    Connect To Database       dbConfigFile=db_default.cfg

LogIn As Admin

    Input Username    ${admin_username}
    Input Password    ${admin_password}
    Submit
    Sleep             3s
LogIn As Operator

    Input Username    ${operator_username}
    Input Password    ${operator_password}
    Submit
    Sleep             3s
LogIn As Viewer

    Input Username    ${viewer_username}
    Input Password    ${viewer_password}
    Submit
    Sleep             3s
Get User List
    Browser.Click                      //*[@id="app"]/section/nav/div/ul/li[6]/div/i
    Browser.Wait For Elements State    //*[@id="app"]/section/nav/div/ul/li[6]/ul/li[3]/span    state=visible
    Browser.Click                      //*[@id="app"]/section/nav/div/ul/li[6]/ul/li[3]/span
    Sleep                              2s
    Get Url                            equal                                                    https://demo.fleet-guard.com/user
Create User Setup
    LogIn As Admin
    Get User List
Close Window
    Browser.Click    //*[@id="app"]/section/div/main/section/div[3]/div/div[1]/button
Create Button is Enabled
    Browser.Wait For Elements State    //*[@id="app"]/section/div/main/section/div[1]/div/form/div[1]/button/span    state=enabled    
    Sleep                              2s
Create Button is hidden
    Browser.Wait For Elements State    //*[@id="app"]/section/div/main/section/div[1]/div/form/div[1]/button/span    state=hidden
    Sleep                              2s
Log Out
    Browser.Click    //*[@id="app"]/section/nav/div/ul/li[9]/span

Click Create Button
    Browser.Click    //*[@id="app"]/section/div/main/section/div[1]/div/form/div[1]/button
Click Confirm Button
    Browser.Click    //*[@id="app"]/section/div/main/section/div[3]/div/div[3]/div/button

Required Fields Warning
    Browser.Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[1]/div/div/div[2]    state=visible
    Browser.Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[2]/div/div/div[2]    state=visible
    Browser.Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[3]/div/div/div[2]    state=visible
    Browser.Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[4]/div/div/div[2]    state=visible
Input Invalid Phone Number
    Browser.Click    //*[@id="MazPhoneNumberInput"]/div[1]/div/label
    Browser.Click    //*[@id="MazPhoneNumberInput"]/div[1]/div/div[2]/div/div[1]/div[212]/button/div[2]
    Fill Text        //*[@id="MazPhoneNumberInput"]/div[2]/div/label                                       12345
Invalid Number Warning
    Browser.Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[3]/div/div/div[2]    state=visible

Input Invalid Email Template
    [Arguments]                        ${email}
    Fill Text                          //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[4]/div/div/div[1]/div/input    ${email}
    Click Confirm Button
    Browser.Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[4]/div/div/div[2]              state=visible
Role Validation
    Browser.Click                      //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[2]/div/div/div/div/input
    Browser.Wait For Elements State    text=Admin                                                                                             state=enabled    
    Browser.Wait For Elements State    text=Operator                                                                                          state=enabled
    Browser.Wait For Elements State    text=Viewer 


Set Email
    ${email}=              Generate random email
    Set Global Variable    ${global_email}          ${email}
    [Return]               ${email}                 

Fill Display Name
    Fill Text    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[1]/div/div/div[1]/input    ${create_user_name}    
Fill Phone Number
    Browser.Click    //*[@id="MazPhoneNumberInput"]/div[1]/div/label
    Browser.Click    //*[@id="MazPhoneNumberInput"]/div[1]/div/div[2]/div/div[1]/div[212]/button/div[2]
    Fill Text        //*[@id="MazPhoneNumberInput"]/div[2]/div/label                                       0977483921
Fill Email

    ${email}=             Set Email
    Set Suite Variable    ${global_email}                                                                                        ${email}
    Fill Text             //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[4]/div/div/div/div/input    ${email}
Fill Duplicate Email
    Fill Text    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[4]/div/div/div/div/input    ${global_email}
Select Role
    [Arguments]      ${role}
    Browser.Click    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[2]/div/div/div/div/input
    Sleep            1s
    Browser.Click    //li[.//text()=${role}]
Create Successful
    ${email}=                     Get Text    //*[@id="app"]/section/div/main/section/div[2]/div[1]/div[3]/table/tbody/tr[1]/td[4]/div/span
    Should Be Equal As Strings    ${email}    ${global_email}
Create Different Role User Template
    [Arguments]             ${role}    
    Click Create Button
    Sleep                   2s
    Fill Display Name
    Fill Phone Number
    Fill Email
    Select Role             ${role}
    Click Confirm Button
    Sleep                   3s
    Create Successful
Create Duplicate Email User
    [Arguments]             
    Click Create Button
    Fill Display Name
    Fill Phone Number
    Fill Duplicate Email
    Select Role             ${admin}
    Click Confirm Button
    Sleep                   5s
Create Failed
    Browser.Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[3]/div/button    state=enabled
Check If User is Verified
    Execute Sql Script            email_verify_script.sql
    Go To                         https://demo.fleet-guard.com/user
    Sleep                         2s
    ${status}=                    Get Text                             //*[@id="app"]/section/div/main/section/div[2]/div[1]/div[3]/table/tbody/tr[1]/td[5]/div
    Should Be Equal As Strings    ${status}                            Verified

Search Template
    [Arguments]       ${filter}
    Run Keyword if    '${filter}'=='${search_role}'                                                              Take Screen Shot    filename=role_search_before
    Run Keyword if    '${filter}'=='${global_email}'                                                             Take Screen Shot    filename=email_search_before
    Run Keyword if    '${filter}'=='${create_user_name}'                                                         Take Screen Shot    filename=name_search_before
    Fill Text         //*[@id="app"]/section/div/main/section/div[1]/div/form/div[2]/div[1]/div/div/div/input    ${filter}
    Browser.Click     //*[@id="app"]/section/div/main/section/div[1]/div/form/div[2]/div[2]/div/div/button
    Sleep             2s
    Run Keyword if    '${filter}'=='${search_role}'                                                              Take Screen Shot    filename=role_search_after
    Run Keyword if    '${filter}'=='${global_email}'                                                             Take Screen Shot    filename=email_search_after
    Run Keyword if    '${filter}'=='${create_user_name}'                                                         Take Screen Shot    filename=name_search_after

Clear Search Area
    Clear Text    //*[@id="app"]/section/div/main/section/div[1]/div/form/div[2]/div[1]/div/div/div/input

Fill Modify Email
    ${modify_email}=    Set Email
    Fill Text           //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[5]/div/div/div/div/input    ${modify_email}
Fill Modify Display Name
    ${random_name}=        Generate random name
    Set Global Variable    ${modify_display_name}                                                                             ${random_name}    
    Fill Text              //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[2]/div/div/div/input    ${random_name}
Click Edit Button
    Browser.Click    //*[@id="app"]/section/div/main/section/div[2]/div[1]/div[5]/div[2]/table/tbody/tr[1]/td[8]/div/button[2]
Edit Select Role
    [Arguments]      ${role}
    Browser.Click    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[3]/div/div/div/div/input
    Sleep            1s
    Browser.Click    //li[.//text()=${role}]
Send Mail Button Should Be Enabled
    Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[5]/div/div/button    state=enabled
Email Field Should Be Readonly
    Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/form/div/div[5]/div/div/div/div/input    state=readonly
Check User status
    ${status}=                    Get Text     //*[@id="app"]/section/div/main/section/div[2]/div[1]/div[3]/table/tbody/tr[1]/td[5]/div
    Should Be Equal As Strings    ${status}    Unverified
Confirm Button Should Be Disabled
    Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[3]/div/button    state=disabled

Delete Button Should Be Enabled
    Wait For Elements State    //*[@id="app"]/section/div/main/section/div[2]/div[1]/div[5]/div[2]/table/tbody/tr[1]/td[8]/div/button[1]    state=enabled
    Click                      //*[@id="app"]/section/div/main/section/div[2]/div[1]/div[5]/div[2]/table/tbody/tr[1]/td[8]/div/button[1]
Click Delete Button
    Click    //*[@id="app"]/section/div/main/section/div[2]/div[1]/div[5]/div[2]/table/tbody/tr[1]/td[8]/div/button[1]
Click YES Button
    Click    //*[@id="app"]/section/div/main/section/div[3]/div/div[3]/div/button
Delete User Invalid Value Template
    [Arguments]    ${value}

    Fill Text                  //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/div/input    ${value}
    Sleep                      1s
    Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[3]/div/button       state=disabled
Fill Valid Value In Input Field
    Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/div/input    state=visible
    Fill Text                  //*[@id="app"]/section/div/main/section/div[3]/div/div[2]/div/div/input    ${valid_delete_input}
    Wait For Elements State    //*[@id="app"]/section/div/main/section/div[3]/div/div[3]/div/button       state=enabled

*** Test Cases ***
TC_001 Create Button UI
    [Documentation]    if user's role is admin or operator,he can see create button and it is clickable
    [Setup]            Create User Setup

    Create Button is Enabled    

TC_002 Create User UI
    [Documentation]            if user fill nothing in required fields, web should show warning
    [Teardown]                 Close Window
    Click Create Button
    Sleep                      1s
    Click Confirm Button
    Required Fields Warning

TC_003 Create User UI
    [Documentation]               phone number validation
    [Teardown]                    Close Window
    Click Create Button
    Input Invalid Phone Number
    Click Confirm Button
    Sleep                         1s
    Invalid Number Warning

TC_004 Create User UI
    [Documentation]    email validation
    [Setup]            Click Create Button
    [Teardown]         Close Window
    [Template]         Input Invalid Email Template

    ${email_2}
    ${email_3}
    ${email_4}
    ${email_5}

TC_005 Create User UI
    [Documentation]    role validation
    [Setup]            Click Create Button
    [Teardown]         Close Window

    Role Validation

TC_006 Create User Functionality
    [Documentation]    If user fill in valid values in required field, they can create a new user
    [Teardown]         
    [Template]         Create Different Role User Template

    ${admin}       
    ${operator}
    ${viewer}

TC_007 Create User Functionality
    [Documentation]                If email is duplicated,web should return error
    [Teardown]                     Close Window
    Create Duplicate Email User    
    Create Failed

TC_008 Search User By Role
    [Documentation]    user can get correct result by filling role
    [Teardown]         Clear Search Area
    [Template]         Search Template                                

    ${search_role}


TC_009 Search User By Email
    [Documentation]    user can get correct result by filling email
    [Teardown]         Clear Search Area
    [Template]         Search Template 


    ${global_email}


TC_010 Search User By Name
    [Documentation]    user can get correct result by filling name
    [Teardown]         Clear Search Area
    [Template]         Search Template 


    ${create_user_name}

TC_011 Edit User Info
    [Documentation]      if user's role is Admin, he can edit every user's name,role,and phone number
    [Teardown]           Close Window
    Click Edit Button
    Sleep                2s


    Fill Modify Display Name
    Fill Modify Email
    Sleep                       1s                         
    Edit Select Role            ${operator}
    Click Confirm Button
    Sleep                       5s
    Click Edit Button           
    Sleep                       1s
    Take Screen Shot            filename=edit_user_info


TC_012 Edit User Info UI
    [Documentation]                       If user’s status is unverified, user can see send button and it is clickable
    [Teardown]                            Close Window 
    Check User status
    Click Edit Button
    Sleep                                 1s
    Send Mail Button Should Be Enabled
TC_013 Edit User Info UI
    [Documentation]                   If user’s status is verified, user can not modify email
    #[Setup]                           Get User List
    [Teardown]                        Close Window 
    Check if user is Verified
    Click Edit Button
    Email Field Should Be Readonly
    Sleep                             1s
    #Send Mail Button Should Be Enabled

TC_014 Edit User Info UI
    [Documentation]    if user did not modify anything,confirm button will be disabled
    [Teardown]         Close Window

    Click Edit Button
    Sleep                                1s
    Confirm Button Should Be Disabled


TC_015 Delete User UI
    [Documentation]                    user can see delete button and it is clickalbe
    [Teardown]                         Close Window
    Delete Button Should Be Enabled

TC_016 Delete User UI
    [Documentation]    if user did not fill anything or fill invalid value in field, YES button should be disabled
    [Setup]            Click Delete Button
    [Teardown]         Close Window
    [Template]         Delete User Invalid Value Template

    ${invalid_delete_input1}     
    ${invalid_delete_input2}     
    ${invalid_delete_input3} 
    ${empty}                     

TC_017 Delete User Functionality
    [Documentation]                    if user fill in valid value, he can delete user successfully
    FOR                                ${index}                                                        IN RANGE    3
    Click Delete Button
    Fill Valid Value In Input Field
    Click YES Button
    Sleep                              3s
    END