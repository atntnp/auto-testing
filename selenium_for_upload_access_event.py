from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import time
from time import sleep
from datetime import datetime
import schedule
import serial
import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S', handlers=[logging.FileHandler('test.log', 'a', 'utf-8'), ])


class LocalStorage:

    def __init__(self, driver):
        self.driver = driver

    def __len__(self):
        return self.driver.execute_script("return window.localStorage.length;")

    def items(self):
        return self.driver.execute_script(
            "var ls = window.localStorage, items = {}; "
            "for (var i = 0, k; i < ls.length; ++i) "
            "  items[k = ls.key(i)] = ls.getItem(k); "
            "return items; ")

    def keys(self):
        return self.driver.execute_script(
            "var ls = window.localStorage, keys = []; "
            "for (var i = 0; i < ls.length; ++i) "
            "  keys[i] = ls.key(i); "
            "return keys; ")

    def get(self, key):
        return self.driver.execute_script("return window.localStorage.getItem(arguments[0]);", key)

    def set(self, key, value):
        self.driver.execute_script(
            "window.localStorage.setItem(arguments[0], arguments[1]);", key, value)

    def has(self, key):
        return key in self.keys()

    def remove(self, key):
        self.driver.execute_script(
            "window.localStorage.removeItem(arguments[0]);", key)

    def clear(self):
        self.driver.execute_script("window.localStorage.clear();")

    def __getitem__(self, key):
        value = self.get(key)
        if value is None:
            raise KeyError(key)
        return value

    def __setitem__(self, key, value):
        self.set(key, value)

    def __contains__(self, key):
        return key in self.keys()

    def __iter__(self):
        return self.items().__iter__()

    def __repr__(self):
        return self.items().__str__()


def upload_event():
    COM_PORT = 'COM3'
    BAUD_RATES = 57600
    ser = serial.Serial(COM_PORT, BAUD_RATES, write_timeout=None)
    ser.timeout = None

    for count in range(0, 10):
        command = f"at$%^&*=49,12,1,0,3\r\n"
        logging.info(str(command))
        try:
            ser.write(command.encode('utf-8'))
            logging.info("執行upload event")
        except serial.serialutil.SerialException as e:
            logging.error(e)
        sleep(60)
    return schedule.cancel_job


def access_event():
    driver = webdriver.Chrome("chromedriver.exe")
    driver.get("https://demo.fleet-guard.com")
    storage = LocalStorage(driver)
    storage.set("deviceId", "eaafbb45-6d8b-4087-849f-692a8bcb731f")
    driver.refresh()
    username = driver.find_element_by_xpath(
        "/html/body/div/div/main/form/div[1]/div/div[1]/input")

    username.send_keys("si-admin@disposeamail.com")
    password = username.find_element_by_xpath(
        "/html/body/div/div/main/form/div[2]/div/div[1]/input")
    password.send_keys("aaaAAA123")
    driver.find_element_by_xpath(
        "/html/body/div/div/main/form/div[3]/button[2]").click()
    sleep(5)
    driver.find_element_by_xpath(
        "/html/body/div/section/nav/div/ul/li[5]").click()
    sleep(3)
    for i in range(1, 11):
        try:
            view = WebDriverWait(driver, 3).until(EC.element_to_be_clickable(
                (By.XPATH, f"/html/body/div[1]/section/div/main/section/div[2]/div[2]/div[1]/div[4]/div[2]/table/tbody/tr[{i}]/td[7]/div/a")))

            view.click()
        except:
            break
        try:
            back = WebDriverWait(driver, 3).until(EC.element_to_be_clickable(
                (By.XPATH, "/html/body/div[1]/section/div/main/div/div[1]/div[1]/button")))
            back.click()
        except:
            break
    sleep(3)

    driver.quit()

    return schedule.cancel_job


# def access_event(access_times):
#     count = 0
#     while(count < access_times):
#         try:
#             view = WebDriverWait(driver, 3).until(EC.element_to_be_clickable(
#                 (By.XPATH, "/html/body/div[1]/section/div/main/section/div[2]/div[2]/div[1]/div[4]/div[2]/table/tbody/tr[1]/td[7]/div/a")))

#             view.click()
#         except:
#             break
#         try:
#             back = WebDriverWait(driver, 3).until(EC.element_to_be_clickable(
#                 (By.XPATH, "/html/body/div[1]/section/div/main/div/div[1]/div[1]/button")))
#             back.click()
#         except:
#             break
#         count += 1
#         # sleep(15)
#     print(count)


# schedule.every(1).seconds.do(upload_event)
schedule.every(5).seconds.do(access_event)

if __name__ == '__main__':
    while True:
        schedule.run_pending()
        sleep(1)
