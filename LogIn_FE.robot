

*** Settings ***
Library             Libraries.py
Library             Browser
Library             OperatingSystem
# Suite Setup       Browser.Open Browser     https://demo.fleet-guard.com    chromium
# Suite Teardown    Browser.Close Browser
*** Variables ***
${base_url}            https://demo.fleet-guard.com
${valid_username}      acloud-admin@disposeamail.com
${valid_password}      acloudadmin
${invalid_username}    wrong@disposeamail.com
${invalid_password}    wrongpassword
*** Keywords ***
Open Browser To Login Page
    Browser.Open Browser     ${base_url}    chromium
    Get Title                equal          uno
    Get Url                  equal          https://demo.fleet-guard.com/login
    Browser.Close Browser
User Interface
    Sleep                              1s
    Browser.Wait For Elements State    //*[@id="app"]/div/main/form/div[3]/button[1]    state=visible
    Browser.Wait For Elements State    //*[@id="app"]/div/main/form/div[3]/button[2]    state=visible
Input Username
    [Arguments]    ${username}
    Fill Text      //*[@id="app"]/div/main/form/div[1]/div/div[1]/input    ${username}
Input Password
    [Arguments]    ${password}
    Fill Text      //*[@id="app"]/div/main/form/div[2]/div/div[1]/input    ${password}
Clear Username
    Clear Text    //*[@id="app"]/div/main/form/div[1]/div/div[1]/input
Clear Password
    Clear Text    //*[@id="app"]/div/main/form/div[2]/div/div[1]/input
Set LocalStorage value
    Browser.Local Storage Set Item    deviceId    eaafbb45-6d8b-4087-849f-692a8bcb731f
    Browser.Reload
Reload Page
    Browser.Reload
Submit
    Browser.Click    //*[@id="app"]/div/main/form/div[3]/button[2]
View Password
    Browser.Click    //*[@id="app"]/div/main/form/div[2]/div/i
LogIn Failed
    Browser.Wait For Elements State    //*[@id="app"]/div/main/form/div[3]/button[2]    state=visible
LogIn Successful
    Browser.Wait For Elements State    //*[@id="app"]/section/nav/div/ul/li[9]/div    state=visible
LogIn Failed Template
    [Arguments]       ${username}    ${password}
    Input Username    ${username}
    Input Password    ${password}
    Submit
    LogIn Failed
    Sleep             3s

Click LogOut
    Browser.Click    //*[@id="app"]/section/nav/div/ul/li[9]
LogOut Successful
    Browser.Wait For Elements State    //*[@id="app"]/div/main/form/div[3]/button[2]    state=visible
    Sleep                              1s
    Get Url                            equal                                            https://demo.fleet-guard.com/login

# *** Test Cases ***
# TC_001 user can get uno page through Chrome

#    Open Browser To Login Page
#    Sleep                         5s

# TC_002 user can see forgot password button and login button

#    User Interface



# TC_003 user should be able to see his password by clicking view icon
#    Clear Password
#    Clear Username
#    Input Password      password
#    Take Screen Shot    filename=view_icon_functionality_before
#    View Password       
#    Take Screen Shot    filename=view_icon_functionality_after
# TC_004 LogIn Failed Test
#    [Template]             LogIn Failed Template
#    [Setup]                Set LocalStorage value    
#    #${valid_username}     ${valid_password}
#    ${invalid_username}    ${invalid_password}
#    ${invalid_username}    ${valid_password}
#    ${valid_username}      ${invalid_password}
#    ${valid_username}      ${empty}
#    ${empty}               ${valid_password}

# TC_005 LogIn Successful Test

#    Input Username      ${valid_username}
#    Input Password      ${valid_password}
#    Submit
#    Sleep               2s
#    LogIn Successful
#    Sleep               2s                   


# TC_006 LogOut Successful Test
#    Click LogOut
#    Sleep                1s
#    LogOut Successful

