*** Settings ***
Library           Libraries.py
Library           Browser
Library           OperatingSystem
Suite Setup       LogIn
Suite Teardown    Browser.Close Browser
*** Variables ***
${group_name}    test_for_robot
${deivice_id}    7612614325113
*** Keywords ***
LogIn

    Browser.Open Browser              https://demo.fleet-guard.com                            chromium
    Browser.Local Storage Set Item    deviceId                                                eaafbb45-6d8b-4087-849f-692a8bcb731f
    Browser.Reload
    Sleep                             1s
    Fill Text                         //*[@id="app"]/div/main/form/div[1]/div/div[1]/input    si-admin@disposeamail.com
    Fill Text                         //*[@id="app"]/div/main/form/div[2]/div/div[1]/input    aaaAAA123
    Browser.Click                     //*[@id="app"]/div/main/form/div[3]/button[2]           

Get to Group Page
    Browser.Click    //*[@id="app"]/section/nav/div/ul/li[4]/span
    Sleep            2s
    Get Url          equal                                           https://demo.fleet-guard.com/device/group
Check Create Button
    Wait For Elements State    //*[@id="app"]/section/div/main/div/div[1]/div/button    state=enabled
Click Create Button
    Click    //*[@id="app"]/section/div/main/div/div[1]/div/button    
Click Next Button
    Click    //*[@id="app"]/section/div/main/div/div[3]/div/div[3]/span/button[2]
Click Next Button in step 1
    Click                      //*[@id="app"]/section/div/main/div/div[3]/div/div[3]/span/button
    Wait For Elements State    //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/form/div[1]/div/div[2]/div[1]/label    state=visible
Click Next Button in step 2
    Click                      //*[@id="app"]/section/div/main/div/div[3]/div/div[3]/span/button[2]
    Wait For Elements State    //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/div/div[1]/div/label    state=visible
Fill Group Name
    Fill Text    //*[@id="group-name-field"]    ${group_name}
Select Group Admin
    Click    //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/form/div/div[1]/div[2]/div/div/div[2]/span/span/i
    Sleep    1s
    #Wait For Elements State    //li[.//text()=SI Operator]                                                                                state=visible
    Click    //li[.//text()='si-operator@disposeamail.com']
Fill Remark
    Fill Text    //*[@id="group-remark-field"]    This is for robot test

Check Step 1 Required Field Warning
    Wait For Elements State    //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/form/div/div[1]/div[1]/div/div[2]    state=visible
    Wait For Elements State    //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/form/div/div[1]/div[2]/div/div[2]    state=visible
Check Step 2 Required Field Warning
    Wait For Elements State    //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/form/div[1]/div/div[3]    state=visible
Select Series
    Click                      //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/form/div[1]/div/div[2]/div/div/div/span/span/i
    Wait For Elements State    //li[.//text()=first release series]
    Click                      //li[.//text()=first release series]
    Sleep                      1s
Select Plan
    Click    //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/form/div[1]/div/div[3]/div/div/table/tbody/tr[3]/td[2]/span/button
Click Back Button in Select Series Step
    Wait For Elements State    //*[@id="app"]/section/div/main/div/div[3]/div/div[3]/span/button[1]              state=enabled
    Click                      //*[@id="app"]/section/div/main/div/div[3]/div/div[3]/span/button[1]
    Wait For Elements State    //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/form/div/div[1]/label[1]    state=visible
Fill Device ID
    Fill Text    //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/div/div[2]/div/div/div[1]/div/div[1]/input    ${device_id}
    Sleep        1s
Check Device
    Click                      //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/div/div[2]/div/div/div[1]/div/div[2]/label/span[1]/span
    Wait For Elements State    //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/div/div[2]/div/div/div[2]/button[2]                        state=enabled
Click Add Triangle Button
    Click                         //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/div/div[2]/div/div/div[2]/button[2]
    Wait For Elements State       //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/div/div[2]/div/div/div[2]/button[2]                        state=disabled
    Wait For Elements State       //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/div/div[2]/div/div/div[3]/div/div[2]/label/span[2]/span    state=visible
    ${add_device_name}=           Get Text                                                                                                         //*[@id="app"]/section/div/main/div/div[3]/div/div[2]/div/div[2]/div/div/div[3]/div/div[2]/label/span[2]/span
    ${name}=                      Catenate                                                                                                         ${device_id}                                                                                                     ' AK11'        
    Should Be Equal As Strings    ${add_device}                                                                                                    ${name}                                                                                                          ${deviceid}
*** Test Cases ***
TC_001 User Can Get to Group Page
    [Documentation]      si role can get group page
    Get to Group Page
TC_002 Create Group UI
    [Documentation]        user can see create button
    Check Create Button
TC_003 Create Group UI
    [Documentation]                        if user did not fill group name or select group admin, web should show warning
    Click Create Button
    Sleep                                  1s
    Click Next Button                      
    Check Step 1 Required Field Warning
TC_004 Create Group Functionality
    [Documentation]                if user fill in valid value in required fields, he can get next step
    Fill Group Name
    Select Group Admin
    Fill Remark
    Click Next Button in step 1
    Sleep                          3s
TC_005 Create Group UI
    [Documentation]                        if user did not select series, web should show warning
    Click Next Button 
    Check Step 2 Required Field Warning

# TC_005 Create Group UI
#    [Documentation]                            user can get previous step by clicking Back button
#    Click Back Button in Select Series Step

# TC_006 Create Group Functionality
#    [Documentation]                if user select valid series, he can get next step
#    Select Series
#    Select Plan
#    Click Next Button in step 2
    # Sleep                          10s
# TC_007 Add Device For Group
#    [Documentation]                    user can search devices and add them to group
#    Fill Device ID
#    Click Add Right Triangle Button

# TC_008 Remove Device For Group
#    [Documentation]    user can search devices and remove them to group