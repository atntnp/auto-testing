*** Settings ***

Library    RequestsLibrary
Library    JSONLibrary
Library    Collections
Library    HttpLibrary.HTTP    
Library    BuiltIn 
Library    Selenium2Library    
Library    String              
Library    Libraries.py
Library    DatabaseLibrary


*** Variables ***
${base_url}                    https://demo.fleet-guard.com
&{headers}                     Content-Type=application/json                                                                                        accept=application/json, text/plain, */*    origin=https://demo.fleet-guard.com    Accept=*/*    Connection=keep-alive    Accept-Encoding=gzip, deflate, br    X-UUID=eaafbb456d8b4087849f692a8bcb731f    
${valid_payload_viewer}        {"msgid":"","data":{"display_name":"Nick","email":"","tel_no":"0988567123","role":"viewer"}}
${valid_payload_operator}      {"msgid":"","data":{"display_name":"Nick","email":"","tel_no":"0988567123","role":"operator"}}
${valid_payload_admin}         {"msgid":"","data":{"display_name":"Nick","email":"","tel_no":"0988567123","role":"admin"}}
${login_payload_admin}         {"msgid":"","data":{"username":"acloud-admin@disposeamail.com","password":"acloudadmin"}}
${login_payload_operator}      {"msgid":"","data":{"username":"acloud-operator@disposeamail.com","password":"acloudoperator"}}
${login_payload_viewer}        {"msgid":"","data":{"username":"acloud-viewer@disposeamail.com","password":"acloudviewer"}}
${duplicate_email_payload}     {"msgid":"","data":{"display_name":"Nick","email":"nick@atrack.com.tw","tel_no":"0988567123","role":"viewer"}}
${invalid_role_payload}        {"msgid":"","data":{"display_name":"Nick","email":"1234@disposeamail.com","tel_no":"0988567123","role":"tester"}}
${blank_email_payload}         {"msgid":"","data":{"display_name":"Nick","email":"","tel_no":"0988567123","role":"viewer"}}
${blank_role_payload}          {"msgid":"","data":{"display_name":"Nick","email":"1234@disposeamail.com","tel_no":"0988567123","role":""}}
${get_info_payload}            {"msgid":"","data":{}}
${update_info_role_payload}    {"msgid":"","data":{"displayname":"New Name","tel_no":"0976315123","role":"viewer"}}
${update_info_payload}         {"msgid":"","data":{"displayname":"New Name123","tel_no":"0976315127"}}
${delete_payload}              {"msgid":"","data":{}}
${companyid}                   1
${admin}                       admin
${operator}                    operator
${viewer}                      viewer
*** Keywords ***
Converting a JSON File
    [Arguments]    ${payload}
    ${data}        Evaluate      json.loads('''${payload}''')    json
    [Return]       ${data}
Set Msgid For Body
    [Arguments]          ${payload}
    ${body}=             Converting a JSON File    ${payload}
    ${uuid}=             Generate uuid
    Set To Dictionary    ${body}                   msgid=${uuid}
    [Return]             ${body}
Set Email For Body
    [Arguments]          ${body}
    #${body}=             Converting a JSON File    ${data}
    ${email}=            Generate random email
    Set To Dictionary    ${body["data"]}          email=${email}
    [Return]             ${body}

LogIn
    [Arguments]            ${login_payload}
    Create Session         LogIn                     ${base_url}         headers=${headers}     verify=True
    ${body}=               Converting a JSON File    ${login_payload}
    ${uuid}=               Generate uuid             
    Set To Dictionary      ${body}                   msgid=${uuid}
    ${log_in_response}=    POST On Session           LogIn               api/v1/users/login/    json=${body}

    [Return]    ${log_in_response}
Create User Failed Template
    [Arguments]                 ${login_payload}      ${data_payload}
    ${log_in_response}=         LogIn                 ${login_payload}
    ${log_in_response_dict}=    Evaluate              json.loads('''${log_in_response.content}''')    json
    ${token}=                   Catenate              Bearer                                          ${log_in_response_dict["data"]["token"]}
    ${body}=                    Set Msgid For Body    ${data_payload}


    ${X-CSRFToken}=      Fetch From Left    ${log_in_response.headers['set-cookie']}[10:]    ;
    Set To Dictionary    ${headers}         X-CSRFToken=${X-CSRFToken}
    Set To Dictionary    ${headers}         Authorization=${token}
    Create Session       create_user        ${base_url}                                      headers=${headers}           verify=True
    ${response}=         POST On Session    create_user                                      api/v1/companies/1/users/    json=${body}    cookies=${log_in_response.cookies}    expected_status=anything

    Should Be Equal As Strings    ${response.status_code}    400
Create User Successful Template
    [Arguments]                   ${login_payload}                         ${data_payload}
    ${log_in_response}=           LogIn                                    ${login_payload}
    ${log_in_response_dict}=      Evaluate                                 json.loads('''${log_in_response.content}''')     json
    ${token}=                     Catenate                                 Bearer                                           ${log_in_response_dict["data"]["token"]}
    ${body}=                      Set Msgid For Body                       ${data_payload}
    ${body}=                      Set Email For Body                       ${body}
    ${X-CSRFToken}=               Fetch From Left                          ${log_in_response.headers['set-cookie']}[10:]    ;
    Set To Dictionary             ${headers}                               X-CSRFToken=${X-CSRFToken}
    Set To Dictionary             ${headers}                               Authorization=${token}
    Create Session                create_user                              ${base_url}                                      headers=${headers}                          verify=True
    ${response}=                  POST On Session                          create_user                                      api/v1/companies/1/users/                   json=${body}                cookies=${log_in_response.cookies}
    Should Be Equal As Strings    ${response.status_code}                  200
    Run Keyword if                '${body["data"]["role"]}'=='admin'       Set Suite Variable                               ${admin_email}                              ${body["data"]["email"]}    
    Run Keyword if                '${body["data"]["role"]}'=='operator'    Set Suite Variable                               ${operator_email}                           ${body["data"]["email"]}
    Run Keyword if                '${body["data"]["role"]}'=='viewer'      Set Suite Variable                               ${viewer_email}                             ${body["data"]["email"]}

Establish DB Connection
    Connect To Database    dbConfigFile=default_db.cfg

    ${queryresult}    Query                   SELECT id FROM sei.auth_user WHERE email = 'taawezob@disposeamail.com';
    Log to console    ${queryresult[0][0]}

Delete User Successful Template
    [Arguments]                 ${login_payload}               ${email}
    ${log_in_response}=         LogIn                          ${login_payload}
    ${log_in_response_dict}=    Evaluate                       json.loads('''${log_in_response.content}''')              json
    ${token}=                   Catenate                       Bearer                                                    ${log_in_response_dict["data"]["token"]}
    ${body}=                    Set Msgid For Body             ${delete_payload}
    ${X-CSRFToken}=             Fetch From Left                ${log_in_response.headers['set-cookie']}[10:]             ;
    Set To Dictionary           ${headers}                     X-CSRFToken=${X-CSRFToken}
    Set To Dictionary           ${headers}                     Authorization=${token}
    Connect To Database         dbConfigFile=default_db.cfg
    ${queryresult}              Query                          SELECT id FROM sei.auth_user WHERE email = '${email}';
    ${delete_userid}=           Set Variable                   ${queryresult[0][0]}

    Create Session                delete_user                ${base_url}                                   headers=${headers}    verify=True
    ${delete_url}=                Set Variable               api/v1/companies/1/users/${delete_userid}/
    ${response}=                  DELETE On Session          delete_user                                   ${delete_url}         json=${body}    cookies=${log_in_response.cookies}
    Should Be Equal As Strings    ${response.status_code}    200

Get User Info Template
    [Arguments]                   ${login_payload}                ${email}                                                  
    ${log_in_response}=           LogIn                           ${login_payload}
    ${log_in_response_dict}=      Evaluate                        json.loads('''${log_in_response.content}''')              json
    ${token}=                     Catenate                        Bearer                                                    ${log_in_response_dict["data"]["token"]}
    ${body}=                      Set Msgid For Body              ${get_info_payload}
    ${X-CSRFToken}=               Fetch From Left                 ${log_in_response.headers['set-cookie']}[10:]             ;
    Set To Dictionary             ${headers}                      X-CSRFToken=${X-CSRFToken}
    Set To Dictionary             ${headers}                      Authorization=${token}
    Connect To Database           dbConfigFile=default_db.cfg 
    ${queryresult}=               Query                           SELECT id FROM sei.auth_user WHERE email = '${email}';
    ${userid}=                    Set Variable                    ${queryresult[0][0]}
    ${get_info_url}=              Set Variable                    api/v1/companies/1/users/${userid}/
    Create Session                get_user_info                   ${base_url}                                               headers=${headers}                          verify=True
    ${response}=                  GET On Session                  get_user_info                                             url=${get_info_url}                         cookies=${log_in_response.cookies}    expected_status=anything
    ${response_dict}=             Evaluate                        json.loads('''${response.content}''')                     json
    ${response_user_id}=          Set Variable                    ${response_dict["data"]["id"]}
    Should Be Equal As Strings    ${response.status_code}         200
    Should Be Equal As Strings    ${userid}                       ${response_user_id }

Update User Info Template
    [Arguments]                 ${login_payload}                ${email}                                                  ${update_info_payload}
    ${log_in_response}=         LogIn                           ${login_payload}
    ${log_in_response_dict}=    Evaluate                        json.loads('''${log_in_response.content}''')              json
    ${token}=                   Catenate                        Bearer                                                    ${log_in_response_dict["data"]["token"]}
    ${body}=                    Set Msgid For Body              ${update_info_payload}
    ${X-CSRFToken}=             Fetch From Left                 ${log_in_response.headers['set-cookie']}[10:]             ;
    Set To Dictionary           ${headers}                      X-CSRFToken=${X-CSRFToken}
    Set To Dictionary           ${headers}                      Authorization=${token}
    Connect To Database         dbConfigFile=default_db.cfg 
    ${queryresult}=             Query                           SELECT id FROM sei.auth_user WHERE email = '${email}';
    ${userid}=                  Set Variable                    ${queryresult[0][0]}
    ${update_info_url}=         Set Variable                    api/v1/companies/1/users/${userid}/
    Create Session              update_user_info                ${base_url}                                               headers=${headers}                          verify=True
    ${response}=                Patch On Session                update_user_info                                          ${update_info_url}                          json=${body}    cookies=${log_in_response.cookies}    expected_status=anything

    Run Keyword if    '${log_in_response_dict["data"]["role"]}'!='admin'    Should Be Equal As Strings    ${response.status_code}    403
    Run Keyword if    '${log_in_response_dict["data"]["role"]}'=='admin'    Should Be Equal As Strings    ${response.status_code}    200

*** Test Cases ***
TC_001 create functionality(admin)

    [Template]                Create User Successful Template    
    ${login_payload_admin}    ${valid_payload_admin}
    ${login_payload_admin}    ${valid_payload_operator}
    ${login_payload_admin}    ${valid_payload_viewer}

TC_002 get info functionality
    [Template]                   Get User Info Template
    ${login_payload_admin}       ${admin_email}
    ${login_payload_operator}    ${admin_email}
    ${login_payload_viewer}      ${admin_email}
TC_003 update info functionality
    [Template]                   Update User Info Template
    ${login_payload_admin}       ${admin_email}               ${update_info_role_payload}
    ${login_payload_admin}       ${operator_email}            ${update_info_role_payload}
    ${login_payload_admin}       ${viewer_email}              ${update_info_role_payload}
    ${login_payload_operator}    ${admin_email}               ${update_info_role_payload}
    ${login_payload_operator}    ${operator_email}            ${update_info_role_payload}
    ${login_payload_operator}    ${viewer_email}              ${update_info_role_payload}
    ${login_payload_viewer}      ${admin_email}               ${update_info_role_payload}
    ${login_payload_viewer}      ${operator_email}            ${update_info_role_payload}
    ${login_payload_viewer}      ${viewer_email}              ${update_info_role_payload}



TC_004 delete functionality(admin)

    [Template]                Delete User Successful Template
    ${login_payload_admin}    ${admin_email}
    ${login_payload_admin}    ${operator_email}
    ${login_payload_admin}    ${viewer_email}




TC_005 create functionality(operator)
    [Template]    Create User Successful Template

    ${login_payload_operator}    ${valid_payload_viewer}
TC_006 delete functionality(operator)

    [Template]    Delete User Successful Template


    ${login_payload_operator}    ${viewer_email}
TC_007 invalid payload or insufficient permission(create)
    [Template]                   Create User Failed Template
    ${login_payload_admin}       ${duplicate_email_payload}
    ${login_payload_operator}    ${duplicate_email_payload}
    ${login_payload_admin}       ${blank_email_payload}
    ${login_payload_operator}    ${blank_email_payload}
    ${login_payload_admin}       ${invalid_role_payload}

    ${login_payload_operator}    ${invalid_role_payload}
    ${login_payload_admin}       ${blank_role_payload}
    ${login_payload_operator}    ${blank_role_payload}
    ${login_payload_operator}    ${valid_payload_operator}
    ${login_payload_operator}    ${valid_payload_admin}



