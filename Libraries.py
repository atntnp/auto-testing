import uuid
import jwt
import random
import string


def generate_uuid():
    result = uuid.uuid4()
    return str(result)


def generate_jwttoken(token_payload):
    token = 'Bearer '+jwt.encode(token_payload,
                                 'stq$#jr%6aj0ha===)*q28*=zxruod8oju^d#r8ptx-z95-a0j', algorithm='HS256').decode()

    return token


def generate_random_email():
    generator = ''.join(random.choice(string.ascii_lowercase)for i in range(8))
    random_name = ''
    for letter in generator:
        random_name += letter
    random_email = random_name + '@disposeamail.com'

    return random_email


def generate_random_name():
    generator = ''.join(random.choice(string.ascii_lowercase)for i in range(8))
    random_name = ''
    for letter in generator:
        random_name += letter

    return random_name
