from xhtml2pdf import pisa
import urllib
from urllib.request import urlopen


def convertHtmlToPdf(sourceHtml, outputFilename):
    resultFile = open(outputFilename, "w+b")
    pisaStatus = pisa.CreatePDF(sourceHtml, resultFile)
    resultFile.close()
    return pisaStatus.err


url = urlopen(
    'file:///C:/Users/Nick/Desktop/auto-testing/User_BE.html')
outputFilename = "User_BE.pdf"
srchhtml = url.read()
if __name__ == "__main__":
    pisa.showLogging()
    convertHtmlToPdf(srchhtml, outputFilename)
