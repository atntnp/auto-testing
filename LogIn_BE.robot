## robot_test_features.robot ###
*** Settings ***
#Resource    robot_test_keywords.robot
Library      RequestsLibrary
Library      JSONLibrary
Library      Collections
Library      HttpLibrary.HTTP             
Library      BuiltIn 
Library      Selenium2Library             
Library      String                       
Library      Libraries.py
*** Variables ***
${base_url}                           https://demo.fleet-guard.com
&{headers}                            Content-Type=application/json                                                                accept=application/json, text/plain, */*    origin=https://demo.fleet-guard.com    Accept=*/*    Connection=keep-alive    Accept-Encoding=gzip, deflate, br    X-UUID=eaafbb456d8b4087849f692a8bcb731f    
${correct_payload}                    {"msgid":"","data":{"username":"acloud-admin@disposeamail.com","password":"acloudadmin"}}
${payload_with_incorrect_username}    {"msgid":"","data":{"username":"wrong@disposeamail.com","password":"acloudadmin"}}
${payload_with_incorrect_pwd}         {"msgid":"","data":{"username":"acloud-admin@disposeamail.com","password":"wrong"}}
${payload_without_username}           {"msgid":"","data":{"password":"acloudadmin"}}
${payload_without_pwd}                {"msgid":"","data":{"username":"acloud-admin@disposeamail.com"}}
${log_out_payload}                    {"msgid":""}
${temp}
*** Keywords ***
Converting a JSON File
    [Arguments]    ${payload}
    ${data}        Evaluate      json.loads('''${payload}''')    json
    [Return]       ${data}

*** Test Cases ***
TC_001 user log in with correct username and password

    Create Session    LogIn    ${base_url}    headers=${headers}

    ${body}=             Converting a JSON File    ${correct_payload}
    ${uuid}=             Generate uuid             
    Set To Dictionary    ${body}                   msgid=${uuid}
    log to console       ${body}
    # ${response}=                  POST On Session            LogIn                 api/v1/users/login/    json=${body}
    # Should Be Equal As Strings    ${response.status_code}    200

    # ${response_dict}    Evaluate    json.loads('''${response.content}''')    json

    # Should Be Equal As Strings    ${response_dict['data']['role']}    admin


# TC_002 user log in with incorrect username but correct password

#    Create Session       LogIn                     ${base_url}                           headers=${headers}
#    ${body}=             Converting a JSON File    ${payload_with_incorrect_username}
#    ${uuid}=             Generate uuid
#    Set To Dictionary    ${body}                   msgid=${uuid}




#    ${response}=    POST On Session    LogIn    api/v1/users/login/    json=${body}    expected_status=anything

#    Should Be Equal As Strings    ${response.status_code}    400

#    ${response_dict}    Evaluate    json.loads('''${response.content}''')    json

#    Should Be Equal As Strings    ${response_dict['error']['message']}    Username or password incorrect
#    Should Be Equal As Strings    ${response_dict['error']['code']}       4010007



# TC_003 user log in with correct username but incorrect password

#    Create Session                LogIn                      ${base_url}                      headers=${headers}
#    ${body}=                      Converting a JSON File     ${payload_with_incorrect_pwd}
#    ${uuid}=                      Generate uuid              
#    Set To Dictionary             ${body}                    msgid=${uuid}
#    ${response}=                  POST On Session            LogIn                            api/v1/users/login/    json=${body}    expected_status=anything
#    Should Be Equal As Strings    ${response.status_code}    400

#    ${response_dict}    Evaluate    json.loads('''${response.content}''')    json

#    Should Be Equal As Strings    ${response_dict['error']['message']}    Username or password incorrect
#    Should Be Equal As Strings    ${response_dict['error']['code']}       4010007



# TC_004 user log in without username

#    Create Session                LogIn                      ${base_url}                    headers=${headers}
#    ${body}=                      Converting a JSON File     ${payload_without_username}
#    ${uuid}=                      Generate uuid              
#    Set To Dictionary             ${body}                    msgid=${uuid}
#    ${response}=                  POST On Session            LogIn                          api/v1/users/login/    json=${body}    expected_status=anything
#    Should Be Equal As Strings    ${response.status_code}    500

#    ${response_dict}    Evaluate    json.loads('''${response.content}''')    json

#    Should Be Equal As Strings    ${response_dict['error']['message']}    'username'
#    Should Be Equal As Strings    ${response_dict['error']['code']}       5000000


# TC_005 user log in without password

#    Create Session                LogIn                      ${base_url}               headers=${headers}
#    ${body}=                      Converting a JSON File     ${payload_without_pwd}
#    ${uuid}=                      Generate uuid              
#    Set To Dictionary             ${body}                    msgid=${uuid}
#    ${response}=                  POST On Session            LogIn                     api/v1/users/login/    json=${body}    expected_status=anything
#    Should Be Equal As Strings    ${response.status_code}    500

#    ${response_dict}    Evaluate    json.loads('''${response.content}''')    json

#    Should Be Equal As Strings    ${response_dict['error']['message']}    'password'
#    Should Be Equal As Strings    ${response_dict['error']['code']}       5000000


# TC_006 user can log out successfully
#    Create Session    LogIn    ${base_url}    headers=${headers}

#    ${body}=               Converting a JSON File    ${correct_payload}
#    ${uuid}=               Generate uuid             
#    Set To Dictionary      ${body}                   msgid=${uuid}
#    ${log_in_response}=    POST On Session           LogIn                                            api/v1/users/login/    json=${body}                          expected_status=anything
#    ${X-csrftoken}=        Fetch From Left           ${log_in_response.headers['set-cookie']}[10:]    ;
#    Set To Dictionary      ${headers}                X-csrftoken=${X-csrftoken}
#    Create Session         LogOut                    ${base_url}                                      headers=${headers}     cookies=${log_in_response.cookies}
#    ${body}=               Converting a JSON File    ${log_out_payload}
#    ${uuid}=               Generate uuid
#    Set To Dictionary      ${body}                   msgid=${uuid}

#    ${log_out_response}=         POST On Session    LogOut                                           api/v1/users/logout/    json=${body}    cookies=${log_in_response.cookies}
#    ${log_out_response_dict}=    Evaluate           json.loads('''${log_out_response.content}''')    json

#    Should Be Equal As Strings    ${log_out_response.status_code}     200
#    Should Be Equal As Strings    ${log_out_response_dict['data']}    logout success




