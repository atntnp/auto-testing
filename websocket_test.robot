*** Settings ***
Library    WebSocketClient
Library    Libraries.py 
*** Variables ***
${request_message}=    {"type":"chat_message","message":{"msgid": "","data":{"deviceid":"356611075962252","receiver":"device","action": "otaapp","amountofapp":"", "app":[ {"app_name":"test","app_version":"7", "filename":"AK11_CMDER_2021","checksum":"E5E6E7E810843E53"} ],"username":"ATrack","password":"5CE249BC","filepath":"/","depend_on_os":"11"}}}
${invalid_message}=    {}

*** Test Cases ***
Echo
    ${my_websocket}=         WebSocketClient.Connect    ws://104.199.228.70:8888/api/v1/devices/356611075962252/
    WebSocketClient.Send     ${my_websocket}            ${request_message}
    ${result}=               WebSocketClient.Recv       ${my_websocket}
    #Should Be Equal          Hello                      ${result}
    log to console           ${result}
    WebSocketClient.Close    ${my_websocket} 